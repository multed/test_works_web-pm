class MailSendController < ApplicationController
  def index
  end
  def sent
    UserMailer.welcome_email.deliver_later
  end
end
