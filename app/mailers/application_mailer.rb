class ApplicationMailer < ActionMailer::Base
  default from: 'mult@multed.com'
  layout 'mailer'
end
