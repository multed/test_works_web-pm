class UserMailer < ApplicationMailer
  default from: 'mult@multed.com'

  def welcome_email
    user_email = 'web@proektmarketing.ru'
    mail(to: user_email, subject: 'test_task_for_backend')
  end
end
