Rails.application.routes.draw do
  get 'mail_send/sent'
  get 'mail_send/index'
  root 'mail_send#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
